import React, { Component } from "react";

export default class ChildComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let coinFlip = [];
    this.props.individualFlips.forEach(outcomes => {
      coinFlip.push(
        <div>
          <img src="coinImage" alt="Coin Image" />
        </div>
      );
    });
    return (
      <div>
        <div>Side: {this.props.side}</div>
        <div>Outcome Percentage: {this.props.outcomePercentage}</div>
        <div>CoinImage: {<img src="coinImage" alt="Coin Image" />}</div>
      </div>
    );
  }
}
