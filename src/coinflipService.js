import rp from "request-promise";

export function getCoinFlip(id) {
  return rp({
    method: "GET",
    uri: "https://coin-api-project.herokuapp.com/coinflip/" + id,
    json: true
  });
}
