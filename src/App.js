import React, { Component } from "react";
import { Button } from "reactstrap";
import { getCoinFlip } from "./coinflipService";
import "./App.css";
import headImage from "./Heads.png";
import tailsImage from "./Tails.png";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 1,
      side: "",
      outcomePercentage: "",
      isLoaded: false,
      individualFlips: []
    };
    this.setId = this.setId.bind(this);
    this.getCoinFlipCall = this.getCoinFlipCall.bind(this);
  }

  getCoinFlipCall() {
    getCoinFlip(this.state.id).then(coinFlip => {
      this.setState({
        side: coinFlip.side,
        outcomePercentage: coinFlip.outcomePercentage,
        individualFlips: coinFlip.individualFlips
      });
      console.log(coinFlip);
    });
  }

  setId(event) {
    this.setState({ id: event.target.value });
  }

  render() {
    let singleCoinFlip = [];
    this.state.individualFlips.forEach(coinFlips => {
      let imageSource =
        coinFlips.coinImage === "Heads.png" ? headImage : tailsImage;
      singleCoinFlip.push(
        <div class="coinFlipOutput">
          Side: {this.state.side}
          <br />
          Flip Number: {this.state.id}
          <br />
          <img src={imageSource} alt={coinFlips.coinImage} />
        </div>
      );
    });

    return (
      <div className="App">
        <header className="App-header">
          <div class="coinFlipOutput">
            <div>
              <div>
                Side: {this.state.side}
                <br />
                Outcome Percentage: {this.state.outcomePercentage}
              </div>
            </div>
          </div>
          <br />
          <label class="label fixed-top">Coin Flip App </label>
          <input
            class="numberInput"
            type="number"
            min="1"
            max="29"
            onChange={this.setId}
            placeholder="Number of Flips"
          />
          <Button class="button" onClick={this.getCoinFlipCall}>
            Flip Coin!
          </Button>
          {singleCoinFlip}
        </header>
      </div>
    );
  }
}
